# NotiPod

iTunes playlist synchronisation to arbitrary directories.

## Overview

Use iTunes on your Mac and wish you could sync your music and playlists to a folder?

NotiPod lets you select iTunes playlists and a destination folder. It then syncs all the music in those playlists (as .m3u files) to the selected folder, removing anything that was already there. It's smart enough to check timestamps and file sizes so only changed or new files get copied.

I use this to sync my music and playlists from iTunes on my laptop to XBMC on my lounge media pc.


## Installation

The latest binary is: [notipod-1.13.app.zip](https://delx.net.au/projects/notipod/notipod-1.13.app.zip).


## TODO

- Option to delete old targets
- Icon
- Choose target on startup based on available paths
- Automatic updates


## Bug reports

Please raise issues on the [Bitbucket project](https://bitbucket.org/delx/notipod/issues?status=new&status=open).
