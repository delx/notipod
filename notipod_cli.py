#!/usr/bin/env python

VERSION = "1.13"

import logging
import optparse
import os
import sys

import libnotipod


def parse_options():
	parser = optparse.OptionParser(version="%prog " + VERSION,
		description = "Synchronise an iTunes playlist with a directory",
		usage = "%prog destination playlist [playlist ...]"
	)
	parser.add_option("-q", "--quiet",
		action="store_true", dest="quiet", default=False)
	parser.add_option("-v", "--verbose",
		action="store_true", dest="verbose", default=False)
	parser.add_option("-n", "--dry-run",
		action="store_true", dest="dry_run", default=False)
	parser.add_option("--itunes-library",
		action="store", dest="itunes_library", default=None)
	parser.add_option("--path-prefix",
		action="store", dest="path_prefix", default="E:\\")

	opts, args = parser.parse_args(sys.argv[1:])
	if len(args) < 2:
		parser.print_usage()
		sys.exit(1)
	opts.dest = args[0].decode("utf-8")
	opts.playlists = args[1:]

	return opts

def main():
	opts = parse_options()

	# Set up logging
	logging.basicConfig(format="%(levelname)s: %(message)s")
	if opts.quiet:
		logging.getLogger().setLevel(logging.CRITICAL)
	elif opts.verbose:
		logging.getLogger().setLevel(logging.DEBUG)
	else:
		logging.getLogger().setLevel(logging.INFO)

	if not os.path.isdir(opts.dest):
		logging.fatal("Destination must be specified as an absolute path.")
		sys.exit(1)

	logging.info("Loading library")
	library = libnotipod.ITunesLibrary.alloc().init()
	gen = library.load_(opts.itunes_library)
	for msg in gen:
		logging.debug(msg)

	for playlist in opts.playlists:
		if not library.has_playlist_name(playlist):
			logging.fatal("Could not find playlist: " + playlist)
			sys.exit(1)

	logging.info("Deleting playlists")
	libnotipod.delete_playlists(opts.dry_run, opts.dest)

	logging.info("Loading playlists")
	all_filenames = []
	for playlist in opts.playlists:
		tracks = library.get_playlist_name(playlist).tracks
		filenames = [library.get_track_filename(trackID) for trackID in tracks]
		all_filenames.extend(filenames)
		libnotipod.export_m3u(
			opts.dry_run,
			opts.dest,
			opts.path_prefix,
			playlist,
			filenames
		)

	logging.info("Synchronising")
	gen = libnotipod.sync(opts.dry_run, library.folder, opts.dest, all_filenames)
	for msg in gen:
		logging.debug(msg)


if __name__ == "__main__":
	main()

