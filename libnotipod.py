#!/usr/bin/env python
# Copyright 2009 James Bunton <jamesbunton@fastmail.fm>
# Licensed for distribution under the GPL version 2, check COPYING for details

import collections
import logging
import os
import shutil
import sys
import urllib

from Foundation import *


def read_plist(filename):
	try:
		data = buffer(open(filename).read())
	except IOError:
		return None
	plist, fmt, err = NSPropertyListSerialization.propertyListFromData_mutabilityOption_format_errorDescription_(data, NSPropertyListMutableContainers, None, None)
	if err is not None:
		errStr = err.encode("utf-8")
		err.release() # Doesn't follow Cocoa conventions for some reason
		raise TypeError(errStr)
	return plist

class Playlist(NSObject):
	def init(self):
		return self

	def set(self, name, pid, ptype, tracks, parent):
		self.name = name
		self.pid = pid
		self.ptype = ptype
		self.children = []
		self.tracks = tracks
		self.parent = parent
		if parent is not None:
			parent.children.append(self)

class ITunesLibrary(NSObject):
	def load_(self, filename=None):
		if filename is None:
			filename = getattr(self, "filename", None)
		if filename is None:
			filename = os.path.expanduser("~/Music/iTunes/iTunes Music Library.xml")
		self.filename = filename
		self.mtime = os.stat(filename).st_mtime
		yield "Reading library..."
		plist = read_plist(os.path.expanduser(filename))
		if plist is None:
			raise Exception("Could not find music library: " + filename)
		self.folder = self.loc2name(plist["Music Folder"])
		pl_tracks = plist["Tracks"]
		pl_lookup = {}
		self.playlists = []
		self.track2playlist = collections.defaultdict(set)
		self.track2filename = {}
		for pl_playlist in plist["Playlists"]:
			playlist = self.make_playlist(pl_playlist, pl_tracks, pl_lookup)
			if not playlist:
				continue
			yield "Read playlist: " + playlist.name
			self.playlists.append(playlist)
			pl_lookup[playlist.pid] = playlist

	def needs_reload(self):
		return os.stat(self.filename).st_mtime > self.mtime

	def loc2name(self, location):
		return urllib.splithost(urllib.splittype(urllib.unquote(location))[1])[1]

	def make_playlist(self, pl_playlist, pl_tracks, pl_lookup):
		if int(pl_playlist.get("Master", 0)):
			return
		kind = int(pl_playlist.get("Distinguished Kind", -1))
		if kind == 26:
			# Don't do genius
			return

		name = pl_playlist["Name"]
		pid = pl_playlist["Playlist Persistent ID"]
		if kind > 0:
			ptype = {
				2: "movies",
				3: "tv-shows",
				4: "music",
				5: "books",
				10: "podcasts",
				19: "purchased",
				22: "itunes-dj",
				31: "itunes-u",
			}.get(kind, "playlist")
		elif pl_playlist.has_key("Smart Info"):
			ptype = "smart-playlist"
		elif int(pl_playlist.get("Folder", 0)):
			ptype = "folder"
		else:
			ptype = "playlist"

		parent = None
		try:
			parent_pid = pl_playlist["Parent Persistent ID"]
			parent = pl_lookup[parent_pid]
		except KeyError:
			pass

		tracks = []
		for item in pl_playlist.get("Playlist Items", []):
			trackID = item["Track ID"]
			item = pl_tracks[str(trackID)]
			self.track2playlist[trackID].add(pid)
			tracks.append(trackID)
			if trackID not in self.track2filename:
				if item["Track Type"] != "File":
					continue
				filename = str(item["Location"])
				filename = self.loc2name(filename)
				filename = filename.decode("utf-8")
				if not filename.startswith(self.folder):
					logging.warn("Skipping: " + filename)
					continue
				filename = strip_prefix(filename, self.folder)
				self.track2filename[trackID] = filename

		playlist = Playlist.alloc().init()
		playlist.set(name, pid, ptype, tracks, parent)
		return playlist

	def has_playlist_name(self, name):
		for p in self.get_playlists():
			if p.name == name:
				return True
		return False

	def get_playlist_name(self, name):
		for playlist in self.get_playlists():
			if playlist.name == name:
				return playlist

	def get_playlist_pid(self, pid):
		for playlist in self.get_playlists():
			if playlist.pid == pid:
				return playlist

	def get_track_filename(self, trackID):
		return self.track2filename.get(trackID, None)

	def get_track_playlists(self, trackID):
		return self.track2playlist.get(trackID, [])

	def get_playlists(self):
		return self.playlists


encoded_names = {}
valid_chars = frozenset("\\/-_.() abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
def encode_filename(filename):
	try:
		return encoded_names[filename]
	except KeyError:
		pass
	orig_filename = filename
	filename = filename.encode("ascii", "ignore")
	filename = "".join(c for c in filename if c in valid_chars)
	if filename in encoded_names:
		a, b = os.path.splitext(filename)
		a += "-dup"
		filename = a + b
	encoded_names[orig_filename] = filename
	return filename

def strip_prefix(s, prefix):
	assert s.startswith(prefix)
	s = s[len(prefix):]
	if s.startswith("/"):
		s = s[1:]
	return s

def mkdirhier(path):
	if os.path.isdir(path):
		return
	paths = [path]
	while path != "/":
		path = os.path.split(path)[0]
		paths.append(path)
	for path in reversed(paths):
		try:
			os.mkdir(path)
		except OSError:
			pass

def delete_playlists(dry_run, dest):
	dest = os.path.join(dest, "-Playlists-")
	try:
		filenames = os.listdir(dest)
	except OSError:
		return

	for filename in filenames:
		if not filename.lower().endswith(".m3u"):
			continue
		filename = os.path.join(dest, filename)
		logging.info("Deleting: " + filename)
		if not dry_run:
			try:
				os.unlink(filename)
			except OSError:
				pass

def export_m3u(dry_run, dest, path_prefix, playlist_name, files):
	dest = os.path.join(dest, "-Playlists-")
	mkdirhier(dest)
	playlist_name = playlist_name.replace("/", "-")
	playlist_file = os.path.join(dest, playlist_name) + ".m3u"
	playlist_file = encode_filename(playlist_file)
	logging.info("Writing: " + playlist_file)

	if dry_run:
		return

	sep = "/"
	if path_prefix.find("\\") > 0:
		sep = "\\"
	if path_prefix[-1] != sep:
		path_prefix += sep

	f = open(playlist_file, "w")
	for filename in files:
		if sep == "\\":
			filename = filename.replace("/", "\\")
		filename = encode_filename(filename)
		f.write("%s%s\n" % (path_prefix, filename))
	f.close()

def sync(dry_run, source, dest, files_to_copy):
	join = os.path.join

	logging.info("Calculating files to sync and deleting old files")
	source = source.encode("utf-8")
	dest = dest.encode("utf-8")
	filemap = {}
	class SyncFile(object): pass
	for f in files_to_copy:
		sf = SyncFile()
		sf.orig_filename = f.encode("utf-8")
		sf.encoded_filename = encode_filename(f)
		filemap[sf.encoded_filename.lower()] = sf
	files_to_copy = set(filemap)

	for dirpath, dirnames, filenames in os.walk(dest):
		full_dirpath = dirpath
		dirpath = strip_prefix(dirpath, dest)

		for filename in filenames:
			filename = join(dirpath, filename)

			# Whenever 'file' is deleted OSX will helpfully remove '._file'
			if not os.path.exists(join(dest, filename)):
				continue

			if filename.lower() in files_to_copy:
				source_filename = filemap[filename.lower()].orig_filename
				sourcestat = os.stat(join(source, source_filename))
				deststat = os.stat(join(dest, filename))
				same_time = abs(sourcestat.st_mtime - deststat.st_mtime) < 5
				same_size = sourcestat.st_size == deststat.st_size
				if same_time and same_size:
					files_to_copy.remove(filename.lower())
					yield "Keep: " + filename
				else:
					yield "Update: " + filename

			elif not filename.startswith("-Playlists-"):
				yield "Delete: " + filename
				if not dry_run:
					os.unlink(join(dest, filename))

		if len(os.listdir(full_dirpath)) == 0:
			yield "Delete: " + dirpath
			if not dry_run:
				os.rmdir(full_dirpath)


	logging.info("Copying new files")
	files_to_copy = list(files_to_copy)
	files_to_copy.sort()
	for filename in files_to_copy:
		yield "Copy: " + filemap[filename].orig_filename
		if not dry_run:
			source_file = join(source, filemap[filename].orig_filename)
			dest_file = join(dest, filemap[filename].encoded_filename)
			mkdirhier(os.path.dirname(dest_file))
			shutil.copy2(source_file, dest_file)


